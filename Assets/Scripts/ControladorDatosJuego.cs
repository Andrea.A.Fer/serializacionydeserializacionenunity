using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class ControladorDatosJuego : MonoBehaviour
{
    public GameObject player;
    public Button btnGuardar;
    public Button btnCargar; 
   
    public string archivoDeGuardado;

    public DatosJuego datosJuego = new DatosJuego();

    void Start()
    {
        if(btnGuardar != null && btnCargar != null)
        {
            btnGuardar.onClick.AddListener(GuardarDatos);
            btnCargar.onClick.AddListener(CargarDatos);
        }
        else
        {
            Debug.LogError("Los botones no est�n asignados en el inspector.");
        }
        
    }

    private void Awake()
    {
        archivoDeGuardado = Application.dataPath + "/datosJuego.json";

        player = GameObject.FindGameObjectWithTag("Player");

        CargarDatos();
    }

    /*private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CargarDatos();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            GuardarDatos();
        }
    }*/

    private void CargarDatos()
    {
        if (File.Exists(archivoDeGuardado))
        {
            string contenido = File.ReadAllText(archivoDeGuardado);
            datosJuego = JsonUtility.FromJson<DatosJuego> (contenido);

            Debug.Log("Posici�n Jugador: " + datosJuego.posicion);

            player.transform.position = datosJuego.posicion;
        }
        else
        {
            Debug.Log("El archivo no existe");
        }
    }

    private void GuardarDatos()
    {
        DatosJuego nuevosDatos = new DatosJuego()
        {
            posicion = player.transform.position,
        };

        string cadenaJSON = JsonUtility.ToJson(nuevosDatos);

        File.WriteAllText(archivoDeGuardado, cadenaJSON);

        Debug.Log("Archivo Guardado");
    }
}
