using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float playerSpeed = 5f;
    public float rotationSpeed = 50f;



    // Update is called once per frame
    void Update()
    {
        float horizontalMove = Input.GetAxis("Horizontal");
        float verticalMove = Input.GetAxis("Vertical");

        Vector3 movementDirection = new Vector3(horizontalMove,0, verticalMove);
        movementDirection.Normalize();

        transform.position = transform.position + movementDirection * playerSpeed * Time.deltaTime;

        if(movementDirection != Vector3.zero) transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementDirection), rotationSpeed * Time.deltaTime);
    }
    

    
}
